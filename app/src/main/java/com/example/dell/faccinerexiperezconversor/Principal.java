package com.example.dell.faccinerexiperezconversor;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class Principal extends AppCompatActivity {
    private Button calcular , calcular1 ;

    private EditText ingresar , ingresar1 ;


    private TextView resultado , resultado1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("Programación movil","Nerexi Perez");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);

        calcular= (Button) findViewById(R.id.convertir);
        calcular1= (Button) findViewById(R.id.convertidor);
        calcular.setOnClickListener(new View.OnClickListener()


        {
            @Override
            public void onClick(View v) {
                ingresar =(EditText) findViewById(R.id.dato1);
                resultado= (TextView) findViewById(R.id.textView);



                try{
                    float grados= Float.parseFloat(ingresar.getText().toString());
                    float  gradoF= 32 + (9* grados/5);


                    resultado.setText("El resultado de grados  Centigrados a Fahrenheit es: " + gradoF);
                } catch (Exception e) {
                     resultado.setText("ingresa solo numeros");
                }
                calcular1.setOnClickListener(new View.OnClickListener()
                {


                    @Override
                    public void onClick(View v) {
                        ingresar1 =(EditText) findViewById(R.id.dato2);
                        resultado1= (TextView) findViewById(R.id.textView1);
                        try{
                            float grados1= Float.parseFloat(ingresar1.getText().toString());
                            float  grado=  (grados1- 32)  * 5 / 9;;


                            resultado.setText("El resultado de grados   Fahrenheit a Centigrados es: " + grado);
                        } catch (Exception e) {
                            resultado1.setText("ingresa solo numeros");
                        }
                    }
                });
            }
        });
    }}
